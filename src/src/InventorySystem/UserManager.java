package InventorySystem;

import java.util.ArrayList;

/**
 * Created by ChrisIsKing on 4/17/16.
 */
public class UserManager {

    private ArrayList<User> UserList;


    public UserManager() {

    }

    public User creatUser(String firstName, String lastName, String password) {
        User newUser = new User(firstName, lastName, password);
        UserList.add(newUser);
        return newUser;
    }

    public void deleteUser(int id) {
        User user = UserList.remove(id);
    }

    public User getUser(int id) {
        User user = UserList.get(id);
        return user;
    }

    public void editUserFirstName(int id, String firstName) {
        User user = getUser(id);
        user.setFirstName(firstName);
    }

    public void editUserLastName(int id, String lastName) {
        User user = getUser(id);
        user.setLastName(lastName);
    }

    public void editUserPassword(int id, String password) {
        User user = getUser(id);
        user.setPassword(password);
    }

}
