package InventorySystem;

/**
 * Created by ChrisIsKing on 4/17/16.
 */
public class SystemUnit {
    private String name;
    private int serial;
    private String type;
    private String brand;

    public SystemUnit(String name, int serial, String type, String brand) {
        this.name = name;
        this.serial = serial;
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
