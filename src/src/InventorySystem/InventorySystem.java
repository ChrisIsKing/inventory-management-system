package InventorySystem;

import java.util.ArrayList;

/**
 * Created by ChrisIsKing on 4/17/16.
 */
public class InventorySystem {
    private ArrayList<SystemUnit> systemUnitList;

    public InventorySystem() {

    }

    public SystemUnit createSystemUnit(String name, int serial, String type, String brand) {
        SystemUnit systemUnit = new SystemUnit(name, serial, type, brand);
        return systemUnit;
    }


}
