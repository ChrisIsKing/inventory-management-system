package InventorySystem;

/**
 * Created by ChrisIsKing on 4/17/16.
 */
public class Manager extends User {
    public Manager(String firstName, String lastName, String password) {
        super(firstName, lastName, password);
    }

    public void deleteUser(int id, UserManager userManager) {
        userManager.deleteUser(id);
    }

    public User createUser(String firstName, String lastName, String password, UserManager userManager) {
        User newUser = userManager.creatUser(firstName, lastName, password);
        return newUser;
    }

    public void editUserFirstName(UserManager userManager, String firstName, int id) {
        userManager.editUserFirstName(id, firstName);
    }

    public void editUserLastName(int id, String lastName, UserManager userManager) {
        userManager.editUserLastName(id, lastName);
    }

    public void editUserPassword(int id, String password, UserManager userManager) {
        userManager.editUserPassword(id, password);
    }

}
